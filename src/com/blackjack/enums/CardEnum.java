package com.blackjack.enums;

public enum CardEnum {
	
	CLUBS_ACE("Ace of clubs", null),
	CLUBS_2("2 of clubs", 2),
	CLUBS_3("3 of clubs", 3),
	CLUBS_4("4 of clubs", 4),
	CLUBS_5("5 of clubs", 5),
	CLUBS_6("6 of clubs", 6),
	CLUBS_7("7 of clubs", 7),
	CLUBS_8("8 of clubs", 8),
	CLUBS_9("9 of clubs", 9),
	CLUBS_10("10 of clubs", 10),
	CLUBS_JACK("Jack of clubs", 10),
	CLUBS_QUEEN("Queen of clubs", 10),
	CLUBS_KING("King of clubs", 10),

	DIAMONDS_ACE("Ace of diamonds", null),
	DIAMONDS_2("2 of diamonds", 2),
	DIAMONDS_3("3 of diamonds", 3),
	DIAMONDS_4("4 of diamonds", 4),
	DIAMONDS_5("5 of diamonds", 5),
	DIAMONDS_6("6 of diamonds", 6),
	DIAMONDS_7("7 of diamonds", 7),
	DIAMONDS_8("8 of diamonds", 8),
	DIAMONDS_9("9 of diamonds", 9),
	DIAMONDS_10("10 of diamonds", 10),
	DIAMONDS_JACK("Jack of diamonds", 10),
	DIAMONDS_QUEEN("Queen of diamonds", 10),
	DIAMONDS_KING("King of diamonds", 10),

	HEARTS_ACE("Ace of hearts", null),
	HEARTS_2("2 of hearts", 2),
	HEARTS_3("3 of hearts", 3),
	HEARTS_4("4 of hearts", 4),
	HEARTS_5("5 of hearts", 5),
	HEARTS_6("6 of hearts", 6),
	HEARTS_7("7 of hearts", 7),
	HEARTS_8("8 of hearts", 8),
	HEARTS_9("9 of hearts", 9),
	HEARTS_10("10 of hearts", 10),
	HEARTS_JACK("Jack of hearts", 10),
	HEARTS_QUEEN("Queen of hearts", 10),
	HEARTS_KING("King of hearts", 10),

	SPADES_ACE("Ace of spades", null),
	SPADES_2("2 of spades", 2),
	SPADES_3("3 of spades", 3),
	SPADES_4("4 of spades", 4),
	SPADES_5("5 of spades", 5),
	SPADES_6("6 of spades", 6),
	SPADES_7("7 of spades", 7),
	SPADES_8("8 of spades", 8),
	SPADES_9("9 of spades", 9),
	SPADES_10("10 of spades", 10),
	SPADES_JACK("Jack of spades", 10),
	SPADES_QUEEN("Queen of spades", 10),
	SPADES_KING("King of spades", 10);

	private String name;
	private Integer blackjackValue;
	
	private CardEnum( String name, Integer blackjackValue ){
		this.name = name;
		this.blackjackValue = blackjackValue;
	}
	
	public String getName() {
		return name;
	}
	public Integer getBlackjackValue() {
		return blackjackValue;
	}

	@Override
	public String toString() {
		return this.getName();
	}
}
