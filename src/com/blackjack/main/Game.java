package com.blackjack.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import com.blackjack.enums.CardEnum;

public class Game {

	private static final String YES = "Y";
	private static final String NO = "N";
	
	private static final int MAX_ACE_POINTS = 11;
	private static final int DEALER_MIN_POINTS = 17;
	private static final int CARD_DECKS_QUANTITY = 3;
	
	private static int dealerMinPoints = 0;
	private static int dealerMaxPoints = 0;
	private static int playerMaxPoints = 0;
	private static int playerMinPoints = 0;
	
	private static List<CardEnum> dealerCards = new ArrayList<>();
	private static List<CardEnum> playerCards = new ArrayList<>();
	private static Map<Integer, CardEnum> mapCards = null;
	
	public static void main(String[] args) throws IOException {
		try{
			mapCards = Game.mixCards(CardEnum.values());
			Game.doFirstHand(mapCards);
			Game.updatePoints(false);
			if( Game.playerNextTable() ){
				Game.updatePoints(true);
				if( Game.dealerMinPoints <= 21 || Game.dealerMaxPoints <= 21 ){
					while( Game.dealerMinPoints < Game.DEALER_MIN_POINTS && Game.dealerMaxPoints < Game.DEALER_MIN_POINTS ){
						System.out.println("# Dealer with points lower than " + Game.DEALER_MIN_POINTS);
						Game.passCardToDealer();
						Game.updatePoints(true);
					}
					if( Game.dealerMinPoints <= 21 || Game.dealerMaxPoints <= 21 ){
						int dealerFinalPoints = Game.getDealerFinalPoints();
						int playerFinalPoints = Game.getPlayerFinalPoints();
						
						Game.presentParticipantsCards(true);
						
						System.out.println("#=========================================================#");
						System.out.println("#                      FINAL SCORE                        #");
						System.out.println("#=========================================================#");
						System.out.println("#   Dealer: " + dealerFinalPoints + " points." );
						System.out.println("#   Player: " + playerFinalPoints + " points." );
						if( dealerFinalPoints == playerFinalPoints ){
							System.out.println("# The result is draw.");
						}else{
							if( dealerFinalPoints > playerFinalPoints ){
								System.out.println("# Dealer is the winner.");
							}else{
								System.out.println("# Player is the winner.");
							}
						}
						System.out.println("");
						System.out.println("#=========================================================#");
					}else{
						System.out.println("Player you won. The dealer burst his points.");
					}
				}else{
					System.out.println("Player you won. The dealer burst his points.");
				}
			}
		}finally{
			Game.restart();
		}
	}
	
	private static void restart(){
		Game.dealerCards = new ArrayList<>();
		Game.playerCards = new ArrayList<>();
	}
	
	private static Map<Integer, CardEnum> mixCards(CardEnum[] cards){
		Map<Integer, CardEnum> map = new HashMap<>();
		Random random = new Random();
		for(int cardDeck = 0; cardDeck < Game.CARD_DECKS_QUANTITY; cardDeck++){
			for( CardEnum cardEnum : cards ){
				Integer randomValue = random.nextInt(Integer.MAX_VALUE);
				while( map.containsKey(randomValue) ){
					randomValue = random.nextInt(Integer.MAX_VALUE);
				}
				map.put(randomValue, cardEnum);
			}
		}
		return map;
	}
	
	private static void doFirstHand(Map<Integer, CardEnum> mapCards){
		List<Entry<Integer, CardEnum>> entries = new ArrayList<>();
		System.out.println("Defining first hand...");
		int count = 0;
		for( Entry<Integer, CardEnum> entry : mapCards.entrySet() ){
			if( count <= 3 ){
				if( count <= 1 ){
					Game.dealerCards.add(entry.getValue());
				}else{
					Game.playerCards.add(entry.getValue());
				}
				entries.add(entry);
			}else{
				break;
			}
			++count;
		}
		
		for(Entry<Integer, CardEnum> entry : entries){
			mapCards.remove(entry.getKey());
		}

		presentParticipantsCards(false);
	}

	private static void presentParticipantsCards(boolean revealHoleCard) {
		System.out.println("#=========================================================#");
		System.out.println("#                    DISTRIBUTED CARDS                    #");
		System.out.println("#=========================================================#");
		System.out.println("# Dealer: " + Game.dealerCards.size() + " cards received.");
		for (int index = 0; index < Game.dealerCards.size(); index++) {
			boolean flagPosition = index > 0 ? true : revealHoleCard;
			if( flagPosition ){
				System.out.println("#\t " + (index + 1) + " - " + Game.dealerCards.get(index).toString());
			}else{
				System.out.println("#\t " + (index + 1) + " - Hole card (classified in the beginning)");
			}
		}
		System.out.println("# Player: " + Game.playerCards.size() + " cards received.");
		for (int index = 0; index < Game.playerCards.size(); index++) {
			System.out.println("#\t " + (index + 1) + " - " + Game.playerCards.get(index).toString());	
		}
		System.out.println("#=========================================================#");
	}
	
	private static void updatePoints(boolean revealHoleCard){
		Game.updateDealerPoints(revealHoleCard);
		Game.updatePlayerPoints();
		System.out.println("\n");
		System.out.println("#=========================================================#");
		System.out.println("#                          POINTS                         #");
		System.out.println("#=========================================================#");
		System.out.println("#   Dealer: Min: " + Game.dealerMinPoints + " / Max: " + Game.dealerMaxPoints + (!revealHoleCard ? " - Pending hole card." : "") );
		System.out.println("#   Player: Min: " + Game.playerMinPoints + " / Max: " + Game.playerMaxPoints );
		System.out.println("#=========================================================#");
	}

	private static void updateDealerPoints(boolean revealHoleCard) {
		Game.dealerMinPoints = 0;
		Game.dealerMaxPoints = 0;
		int dealerAces = 0;
		for (int index = 0; index < Game.dealerCards.size(); index++) {
			boolean flagPosition = index > 0 ? true : revealHoleCard;
			if( flagPosition ){
				CardEnum cardEnum = Game.dealerCards.get(index);
				if( cardEnum.getBlackjackValue() != null ){
					Game.dealerMinPoints += cardEnum.getBlackjackValue();
				}else{
					++dealerAces;
				}
			}
		}
		Game.dealerMaxPoints = (Game.dealerMinPoints + (dealerAces * Game.MAX_ACE_POINTS));
		Game.dealerMinPoints += dealerAces;
	}
	
	private static void updatePlayerPoints(){
		Game.playerMinPoints = 0;
		Game.playerMaxPoints = 0;
		int playerAces = 0;
		for( CardEnum cardEnum : Game.playerCards ){
			if( cardEnum.getBlackjackValue() != null ){
				Game.playerMinPoints += cardEnum.getBlackjackValue();
			}else{
				++playerAces;
			}
		}
		Game.playerMaxPoints += (Game.playerMinPoints + (playerAces * Game.MAX_ACE_POINTS));
		Game.playerMinPoints += playerAces;
	}
	
	private static boolean checkPlayerPoints(){
		updatePlayerPoints();
		return !(Game.playerMinPoints > 21 && Game.playerMaxPoints > 21);
	}
	
	private static boolean playerNextTable() throws IOException{
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
		boolean askMoreCards = true;
		while( askMoreCards ){
			if( Game.checkPlayerPoints() ){
				System.out.print("Would you like more cards ? (Y - yes / N - No): ");
				String optionSelected = bufferedReader.readLine();
				switch( optionSelected ){
					case YES:{
						passCardToPlayer();
						break;
					}
					case NO:{
						askMoreCards = false;
					}
				}
			}else{
				System.out.println("Player you lost. You burst your points.");
				return false;
			}
		}
		return true;
	}

	private static void passCardToPlayer() {
		if( mapCards.size() > 0 ){
			Iterator<Entry<Integer, CardEnum>> iterator = mapCards.entrySet().iterator();
			Entry<Integer, CardEnum> entry = iterator.next();
			Game.playerCards.add(entry.getValue());
			iterator.remove();
			Game.presentParticipantsCards(false);
			Game.updatePoints(false);
		}
	}
	
	private static void passCardToDealer(){
		if( mapCards.size() > 0 ){
			Iterator<Entry<Integer, CardEnum>> iterator = mapCards.entrySet().iterator();
			Entry<Integer, CardEnum> entry = iterator.next();
			Game.dealerCards.add(entry.getValue());
			iterator.remove();
			Game.presentParticipantsCards(true);
			Game.updatePoints(true);
		}
	}
	
	private static int getPlayerFinalPoints(){
		int returnValue = 0;
		if( Game.playerMinPoints <= 21 || Game.playerMaxPoints <= 21 ){
			if( Game.playerMinPoints <= 21 && Game.playerMaxPoints <= 21 ){
				if( Game.playerMinPoints < Game.playerMaxPoints ){
					returnValue = Game.playerMaxPoints;
				}else{
					returnValue = Game.playerMinPoints;
				}
			}else{
				if( Game.playerMinPoints <= 21 ){
					returnValue = Game.playerMinPoints; 
				}else{
					returnValue = Game.playerMaxPoints;
				}
			}
		}else{
			if( Game.playerMinPoints == Game.playerMaxPoints ){
				returnValue = Game.playerMinPoints;
			}else{
				if( Game.playerMinPoints < Game.playerMaxPoints ){
					returnValue = Game.playerMaxPoints;
				}else{
					returnValue = Game.playerMinPoints;
				}
			}
		}
		return returnValue;
	}
	
	private static int getDealerFinalPoints(){
		int returnValue = 0;
		if( Game.dealerMinPoints <= 21 || Game.dealerMaxPoints <= 21 ){
			if( Game.dealerMinPoints <= 21 && Game.dealerMaxPoints <= 21 ){
				if( Game.dealerMinPoints < Game.dealerMaxPoints ){
					returnValue = Game.dealerMaxPoints;
				}else{
					returnValue = Game.dealerMinPoints;
				}
			}else{
				if( Game.dealerMinPoints <= 21 ){
					returnValue = Game.dealerMinPoints; 
				}else{
					returnValue = Game.dealerMaxPoints;
				}
			}
		}else{
			if( Game.dealerMinPoints == Game.dealerMaxPoints ){
				returnValue = Game.dealerMinPoints;
			}else{
				if( Game.dealerMinPoints < Game.dealerMaxPoints ){
					returnValue = Game.dealerMaxPoints;
				}else{
					returnValue = Game.dealerMinPoints;
				}
			}
		}
		return returnValue;
	}
	
}
